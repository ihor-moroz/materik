$(document).ready(function() {

    /*anchor up page*/
    anchor('._anchor');
    function anchor(icon){
        $(icon).hide();
        $(function () {
            $(window).scroll(function () {
                ($(this).scrollTop() > 100) ? $(icon).fadeIn() : $(icon).fadeOut();
            });
            $(icon).click(function () {
                $('body,html').animate({ scrollTop: 0}, 1000);
                return false;
            });
        });
    }
    var _text = true;
    $('#text').click( function(){
        if(_text === false)
        {
            $('#textrun').removeClass('hidden').addClass('_actions-marquee');
            _text = true;
            return false;
        }
        if(_text === true)
        {
            console.log(_text);
            $('#textrun').removeClass('_actions-marquee').addClass('hidden');
            _text = false;
            return false;
        }
    });
    $('#play').click(function(){
        console.log('play');
        $('._actions-marquee span').css('animation-play-state', 'running');
        $(this).addClass('hidden');
        $('#stop').removeClass('hidden');
    });
    $('#stop').click(function(){
        console.log('stop');
        $('._actions-marquee span').css('animation-play-state', 'paused');
        $(this).addClass('hidden');
        $('#play').removeClass('hidden');

    });




});
